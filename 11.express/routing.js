var express = require('express');

var app = express();
// Requests on /
app.get('/', function (request, response) {
    console.log('Receive GET request on /.');
    response.send('GET responsed on /');
});
app.post('/', function (request, response) {
    console.log('Receive POST request on /.');
    response.send('POST responsed on /');
});
// Requests on /del_user
app.delete('/del_user', function (request, response) {
    console.log('Receive DELETE request on /del_user.');
    response.send('User is removed.');
});
// Requests on /sp*n
app.get('/sp*n', function (request, response) {
    console.log('Receive GET request on /sp*n.');
    response.send('Request matched with pattern sp*n.');
});

var server = app.listen(8081, function () {
    var host = server.address().address
    var port = server.address().port
    console.log("Routing app listening at http://%s:%s", host, port)
});