var express = require('express');
var body_parser = require('body-parser');

var app = express();
var urlencoded_parser = body_parser.urlencoded({ 
    extended: false 
});

app.get('/',  function (request, response) {
    response.sendFile( __dirname + "\\form.html" );
});
app.get('/do_query', function (request, response) {
    response_body = {
       first_name: request.query.first_name,
       last_name: request.query.last_name
    };
    console.log(response_body);
    response.end(JSON.stringify(response_body));
})
app.post('/do_query', urlencoded_parser, function (request, response) {
    response_body = {
       first_name: request.body.first_name,
       last_name: request.body.last_name
    };
    console.log(response_body);
    response.end(JSON.stringify(response_body));
})

var server = app.listen(8081, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log("App listening at http://%s:%s", host, port);
});