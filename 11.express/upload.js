// WORK IN PROGRESS
// TypeError: Cannot read property 'file' of undefined

var express = require('express');
var body_parser = require('body-parser');
var multer  = require('multer');
var fs = require("fs");

var upload = multer({ dest: __dirname + '\\tmp'});

var app = express();
app.use(express.static('public'));
app.use(body_parser.urlencoded({ extended: false }));

app.get('/',  function (request, response) {
    response.sendFile( __dirname + "\\upload.html" );
});
app.post('/upload_file', function (request, response) {
    console.log(request.files.file.name);
    console.log(request.files.file.path);
    console.log(request.files.file.type);
    var file_path = __dirname + "\\tmp\\" + request.files.file.name;

    fs.readFile(req.files.file.path, function (err, data) {
        fs.writeFile(file_path, data, function (err) {
            if (err) {
                console.log(err);
            } else {
                response_body = {
                    message:'File uploaded successfully',
                    file_path: "\\tmp\\" + request.files.file.name
                };
                console.log(response_body);
            }
            response.end(JSON.stringify(response_body));
        });
    });
});

var server = app.listen(8081, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log("App listening at http://%s:%s", host, port);
});