var fs = require("fs");

fs.stat('7.file_system\\pandora.txt', function (err, stats) {
    if (err) {
        return console.error(err);
    }
    console.log(stats);
    console.log("isFile ? " + stats.isFile());
    console.log("isDirectory ? " + stats.isDirectory());
})

fs.open('7.file_system\\pandora.txt', 'r+', function (err, text) {
    if (err) {
        return console.error(err);
    }
    console.log("File opened successfully."); 
})

console.log('World ends...');