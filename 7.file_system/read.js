var fs = require('fs');

// Asynchronous Read
fs.readFile('7.file_system\\pandora.txt', function (err, data) {
    if (err) {
        return console.error(err);
    }
    console.log("Asynchronous read: \n" + data.toString());
})

// Synchronous Read
var text = fs.readFileSync('7.file_system\\pandora.txt');
console.log("Synchronous read: \n" + text.toString());

console.log('World ends...');