var fs = require('fs');
var text = '';

var reader_stream = fs.createReadStream('5.streams\\pandora.txt');
reader_stream.setEncoding('utf8');
reader_stream.on('data', function(chunk) {
    text += chunk;
})
reader_stream.on('end', function() {
    console.log(text)
})
reader_stream.on('error', function(err) {
    console.log(err.stack)
})

console.log('World ends...');
