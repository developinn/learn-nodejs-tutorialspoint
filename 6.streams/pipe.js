var fs = require('fs');
var reader_stream = fs.createReadStream('5.streams\\pandora.txt');
var writer_stream = fs.createWriteStream('5.streams\\another_pandora.txt')
reader_stream.pipe(writer_stream);

var zip = require('zlib');
reader_stream.pipe(zip.createGzip()).pipe(fs.createWriteStream('5.streams\\zipped_pandora.txt.gz'))

console.log('World ends...');