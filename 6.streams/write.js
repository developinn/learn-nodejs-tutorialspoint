var fs = require('fs');
var text = 'Say something to me...';

var writer_stream = fs.createWriteStream('5.streams\\river.txt')
writer_stream.write(text, 'utf8');
writer_stream.end();
writer_stream.on('finish', function() {
    console.log('Data written.')
})
writer_stream.on('error', function(err) {
    console.log(err.stack)
})

console.log('World ends...');