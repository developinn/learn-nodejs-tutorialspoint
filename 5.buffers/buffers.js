var buffer = new Buffer(256);

var size = buffer.write('My name is Bruce Wayne.');
console.log('Octets occupied: ' + size);
console.log(buffer.toString('utf8', 0, size));

size = buffer.write('My name is Sittinut Chivakunakorn.');
console.log('Octets occupied: ' + size);
console.log(buffer.toString('utf8', 0, size));

console.log('World ends...');