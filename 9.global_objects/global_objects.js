// Global variables
console.log(__filename + "\n");
console.log(__dirname + "\n");

// Global functions
function say_hi(){
    console.log( "Hello, World!");
 }
 setTimeout(say_hi, 2000); // Wait 2 secs to call function
 var time =  setTimeout(say_hi, 2000); // Set timeout
 clearTimeout(time); // Clear timeout immediately