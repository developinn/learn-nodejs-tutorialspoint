var events = require("events");

var eventEmitter = new events.EventEmitter();
var connectHandler = function connected() {
    console.log('Connection established.');
    eventEmitter.emit('send_data');
}
eventEmitter.on('connect', connectHandler)
var dataReceivedHandler = function received() {
    console.log('Data received.');
}
eventEmitter.on('send_data', dataReceivedHandler);

eventEmitter.emit('connect')
console.log('World ends...')