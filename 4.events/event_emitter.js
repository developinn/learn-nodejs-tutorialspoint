var events = require('events');

var eventEmitter = new events.EventEmitter();
var listenerA = function listenerA() {
    console.log('Hi, this is A.');
}
var listenerB = function listenerB() {
    console.log('Hi, this is B.');
}
eventEmitter.addListener('connect', listenerA);
eventEmitter.on('connect', listenerB);

var listenerCount = require('events').EventEmitter.listenerCount(eventEmitter, 'connect');
console.log('Listener(s) on \'connect\' is counted to ' + listenerCount + '.')
eventEmitter.emit('connect')

eventEmitter.removeListener('connect', listenerB)
console.log('Listener B removed.')
eventEmitter.emit('connect')