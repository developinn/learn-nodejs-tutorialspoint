var express = require('express');
var fs = require('fs');
var body_parser = require('body-parser');

var json_parser = body_parser.json();
var app = express();

app.get('/user',  function (req, res) {
    fs.readFile(__dirname + "\\data\\users.json", "utf8", function (err, data) {
        if (err) {
            console.error(err);
        } else {
            console.log(data);
            res.end(data);
        }
    })
});

app.get('/user/:name', function (req, res) {
    fs.readFile(__dirname + "\\data\\users.json", "utf8", function (err, data) {
        if (err) {
            console.error(err);
        } else {
            data = JSON.parse(data);
            user = data[req.params.name];
            console.log(JSON.stringify(user));
            res.end(JSON.stringify(user));
        }
    })
})

app.post('/user/add', json_parser, function (req, res) {
    fs.readFile(__dirname + "\\data\\users.json", "utf8", function (err, data) {
        if (err) {
            console.error(err);
        } else {
            data = JSON.parse(data);
            new_user = req.body;
            data[new_user["user"]["name"]] = new_user["user"];
            res.end(JSON.stringify(data));
        }
    })
});

app.delete('/user/delete', json_parser, function (req, res) {
    fs.readFile(__dirname + "\\data\\users.json", "utf8", function (err, data) {
        if (err) {
            console.error(err);
        } else {
            data = JSON.parse( data );
            delete data[req.body["name"]];
            console.log( data );
            res.end(JSON.stringify(data));
        }
    })
});

var server = app.listen(8081, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log("App listening at http://%s:%s", host, port);
});